let http = require ('http');
let users = [
    {
        "name": "Chella Montez",
        "email": "cmontez@gmail.com"
    },
    {
        "name": "Elon Musk",
        "email": "emusk@gmail.com"
    }
];

let port = 4000;

let server = http.createServer(function(request, response){

    if (request.url == '/users' && request.method == 'GET'){
        response.writeHead(200, {'content-Type': 'application/json'});
        response.write(JSON.stringify(users));
        response.end();
    };

    if (request.url == '/users' && request.method == 'POST'){
        console.log("yyooo");
       
    /*1. Declare a placeholder variable in order to be re-assigned later on.
         this variable will be assigned the data inside the body from postman.
    */ let request_body = '';
       
    // 2. when data is detected, run a function that assigns that  data to the empty request_body variable.
        request.on('data', function(data){ // request.on can have either of three values (data,  error, end)
            request_body += data
        });
    //  3. Before the request end, we want to add new data to our existing users array of objects
        request.on('end', function(){
            console.log(typeof request_body);

    // 4. Converts the request_body from JSON to a js object, then assign tha converted value back to the request_body var.
            request_body = JSON.parse(request_body);

    // 5. Declares a new_user variable signifying the new data that come from the Postman body. 
            let new_user = {
                "name": request_body.name,
                "email": request_body.email
            }
    // 6. Add the new_user variable (Object) into the users array.
            users.push(new_user);
            console.log(users);

    // 7. Write the headers for the response, Make sure the content type is application/json since we are writing/returning a JSON 
            response.writeHead(200, {'content-Type': 'application/json'});
            response.write(JSON.stringify(new_user));
            response.end();
        });
    };
})
server.listen(port);
console.log(`Server is now running at localhost: ${port}`);

