/*
HTTP Methods
    1. Get - Retrieve resources
    2. Post - sends data for creating a resource
    3. Put - updating data
    4. Delete - deleting data

POSTMAN CLIENT
    - to improve the experience of sending request, inspecting response and debugging.
*/

const http = require ('http');
const port = 4000

const server = http.createServer(function (request, response){

    if (request.url == '/items' && request.method == 'GET'){
        response.writeHead(200, {'content-Type': 'text/plain'});
        response.end("Data retrieve from the database");
    };
    if (request.url == '/items' && request.method == 'POST'){
        response.writeHead(200, {'content-Type': 'text/plain'});
        response.end("Data sent to the database");
    };
   
})
server.listen(port);
console.log(`Server is now running at localhost: ${port}`);

